from flask import Flask, request, jsonify
from flask_cors import CORS
import pymysql.cursors

app = Flask(__name__)
CORS(app)

# Database connection
connection = pymysql.connect(host='localhost',
                             user='root',
                             password='password',
                             database='testdb',
                             cursorclass=pymysql.cursors.DictCursor)

@app.route('/user', methods=['POST'])
def create_user():
    data = request.json
    with connection.cursor() as cursor:
        sql = "INSERT INTO `users` (`name`, `email`) VALUES (%s, %s)"
        cursor.execute(sql, (data['name'], data['email']))
        connection.commit()
    return jsonify(data), 201

@app.route('/user/<int:user_id>', methods=['GET'])
def get_user(user_id):
    with connection.cursor() as cursor:
        sql = "SELECT * FROM `users` WHERE `id`=%s"
        cursor.execute(sql, (user_id,))
        user = cursor.fetchone()
    if user:
        return jsonify(user)
    return jsonify(error='User not found'), 404

@app.route('/users', methods=['GET'])
def get_all_users():
    with connection.cursor() as cursor:
        sql = "SELECT * FROM `users`"
        cursor.execute(sql)
        users = cursor.fetchall()
    return jsonify(users)

if __name__ == '__main__':
    app.run(port=5000)
