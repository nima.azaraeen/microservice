import requests
from flask import Flask, request, jsonify
from flask_cors import CORS
import pymysql.cursors

app = Flask(__name__)
CORS(app)

# Database connection
connection = pymysql.connect(host='localhost',
                             user='root',
                             password='password',
                             database='testdb',
                             cursorclass=pymysql.cursors.DictCursor)

@app.route('/order', methods=['POST'])
def create_order():
    data = request.json
    user_id = data.get('user_id')

    # Communicate with the user service to get user info
    response = requests.get(f'http://127.0.0.1:5000/user/{user_id}')
    if response.status_code != 200:
        return jsonify(error='User not found'), 404

    user = response.json()
    with connection.cursor() as cursor:
        sql = "INSERT INTO `orders` (`user_id`, `items`) VALUES (%s, %s)"
        cursor.execute(sql, (user_id, str(data.get('items'))))
        connection.commit()
    return jsonify(user_id=user_id, items=data.get('items')), 201

@app.route('/order/<int:order_id>', methods=['GET'])
def get_order(order_id):
    with connection.cursor() as cursor:
        sql = "SELECT * FROM `orders` WHERE `id`=%s"
        cursor.execute(sql, (order_id,))
        order = cursor.fetchone()
    if order:
        return jsonify(order)
    return jsonify(error='Order not found'), 404

@app.route('/orders', methods=['GET'])
def get_all_orders():
    with connection.cursor() as cursor:
        sql = "SELECT * FROM `orders`"
        cursor.execute(sql)
        orders = cursor.fetchall()
    return jsonify(orders)

if __name__ == '__main__':
    app.run(port=5001)
