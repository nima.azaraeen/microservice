from flask import Flask, request, jsonify
from flask_cors import CORS

app = Flask(__name__)

# IMPORTANT! this line is added for simplicity of education, DO NOT USE IN PRODUCTION
CORS(app)

users = {}

@app.route('/user', methods=['POST'])
def create_user():
    data = request.json
    user_id = len(users) + 1
    users[user_id] = data
    return jsonify(users), 201

@app.route('/user/<int:user_id>', methods=['GET'])
def get_user(user_id):
    user = users.get(user_id)
    if user:
        return jsonify(id=user_id, **user)
    return jsonify(error='User not found'), 404

@app.route('/user-mehran/<int:user_id>', methods=['GET'])
def get_mehran_user(user_id):
    user = users.get(user_id)
    if user:
        sahak_half_user = {"name":user['name']}
        return jsonify(id=user_id, **sahak_half_user)
    return jsonify(error='User not found'), 404

@app.route('/users', methods=['GET'])
def get_all_users():
    return jsonify(users)

if __name__ == '__main__':
    app.run(port=5000)