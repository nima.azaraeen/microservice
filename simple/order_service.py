import requests
from flask import Flask, request, jsonify
from flask_cors import CORS


app = Flask(__name__)

# IMPORTANT! this line is added for simplicity of education, DO NOT USE IN PRODUCTION
CORS(app)

orders = {}

@app.route('/order', methods=['POST'])
def create_order():
    data = request.json
    order_id = len(orders) + 1
    user_id = data.get('user_id')
    
    # Communicate with the user service to get user info
    response = requests.get(f'http://127.0.0.1:5000/user-mehran/{user_id}')
    if response.status_code != 200:
        return jsonify(error='User not found'), 404
    
    user = response.json()
    orders[order_id] = {'user': user, 'items': data.get('items')}
    return jsonify(id=order_id, **orders[order_id]), 201

@app.route('/order/<int:order_id>', methods=['GET'])
def get_order(order_id):
    order = orders.get(order_id)
    if order:
        return jsonify(id=order_id, **order)
    return jsonify(error='Order not found'), 404

@app.route('/all', methods=['GET'])
def get_all_orders():
    return jsonify(orders)

@app.route('/order/<string:val>', methods=['GET'])
def returnval(val):
    return jsonify(val)

if __name__ == '__main__':
    app.run(port=5001)